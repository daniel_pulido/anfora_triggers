/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

//Microsoft Dynamics AX for Retail POS Plug-ins 
//The following project is provided as SAMPLE code. Because this software is "as is," we may not provide support services for it.

using System.ComponentModel.Composition;
using System.Data.SqlClient;
using LSRetailPosis;
using LSRetailPosis.POSProcesses;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using Microsoft.Dynamics.Retail.Pos.Contracts.Triggers;
using LSRetailPosis.Transaction;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using LSRetailPosis.Transaction.Line.TenderItem;
using System;
using System.Windows.Forms;
using LSRetailPosis.Transaction.Line.SaleItem;
using System.Data;
using LSRetailPosis.Settings;
using System.Text;

namespace Microsoft.Dynamics.Retail.Pos.TransactionTriggers
{
    [Export(typeof(ITransactionTrigger))]
    public class TransactionTriggers : ITransactionTrigger
    {
        [Import]
        public IApplication Application { get; set; }
        private DAC odac;
        string employeeid = string.Empty;
        string employeename = string.Empty;
        StringBuilder reportLayout;
        private const int paperWidth = 55;
        private static readonly string singleLine = string.Empty.PadLeft(paperWidth, '-');
        private static readonly string lineFormat = ApplicationLocalizer.Language.Translate(7060);
        private static readonly string currencyFormat = ApplicationLocalizer.Language.Translate(7061);
        private static readonly string typeFormat = ApplicationLocalizer.Language.Translate(7062);

        #region Constructor - Destructor

        public TransactionTriggers()
        {

            // Get all text through the Translation function in the ApplicationLocalizer
            // TextID's for TransactionTriggers are reserved at 50300 - 50349
        }

        #endregion

        public void BeginTransaction(IPosTransaction posTransaction)
        {
            LSRetailPosis.ApplicationLog.Log("TransactionTriggers.BeginTransaction", "When starting the transaction...", LSRetailPosis.LogTraceLevel.Trace);
        }

        /// <summary>
        /// Triggered during save of a transaction to database.
        /// </summary>
        /// <param name="posTransaction">PosTransaction object.</param>
        /// <param name="sqlTransaction">SqlTransaction object.</param>
        /// <remarks>
        /// Use provided sqlTransaction to write to the DB. Don't commit, rollback transaction or close the connection.
        /// Any exception thrown from this trigger will rollback the saving of pos transaction.
        /// </remarks>
        public void SaveTransaction(IPosTransaction posTransaction, SqlTransaction sqlTransaction)
        {
            ApplicationLog.Log("TransactionTriggers.SaveTransaction", "Saving a transaction.", LogTraceLevel.Trace);

            //Example:
            //if (posTransaction is IRetailTransaction)
            //{
            //    string commandString = "INSERT INTO PARTNER_CUSTOMTRANSACTIONTABLE VALUES (@VAL1)";

            //    using (SqlCommand sqlCmd = new SqlCommand(commandString, sqlTransaction.Connection, sqlTransaction))
            //    {
            //        sqlCmd.Parameters.Add(new SqlParameter("@VAL1", posTransaction.PartnerData.TestData));
            //        sqlCmd.ExecuteNonQuery();
            //    }
            //}
        }

        public void PreEndTransaction(IPreTriggerResult preTriggerResult, IPosTransaction posTransaction)
        {
            LSRetailPosis.ApplicationLog.Log("TransactionTriggers.PreEndTransaction", "When concluding the transaction, prior to printing and saving...", LSRetailPosis.LogTraceLevel.Trace);

            #region GRW Add SALESPERSON
            string defaultSalesPersonId = string.Empty;
            string defaultSalesPersonName = string.Empty;
            try
            {
                int verificaVendedor = 0;
                if (posTransaction is RetailTransaction || posTransaction is CustomerOrderTransaction)
                {
                    RetailTransaction retailTransaction = posTransaction as RetailTransaction;
                    if (retailTransaction.EntryStatus != LSRetailPosis.Transaction.PosTransaction.TransactionStatus.Voided)
                    {
                        if (retailTransaction.SaleItems.Count > 0)
                        {
                            DAC odac = new DAC(Application.Settings.Database.Connection);
                            DataTable dtSalesPersonId = odac.getDefaultSalesPerson(posTransaction.StoreId);
                            if (dtSalesPersonId.Rows.Count > 0)
                            {
                                defaultSalesPersonId = dtSalesPersonId.Rows[0][0].ToString();
                                int.TryParse(dtSalesPersonId.Rows[0]["GRWASKSALESPERSON"].ToString(), out verificaVendedor);
                            }
                            else
                            {
                                Application.Services.Dialog.ShowMessage("Error al cargar el vendedor default. Contacte con sistemas. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            foreach (SaleLineItem item in retailTransaction.SaleItems)
                            {
                                if (string.IsNullOrEmpty(item.SalesPersonId) && string.IsNullOrEmpty(item.SalespersonName))
                                {
                                    retailTransaction.SalesPersonId = "";
                                    retailTransaction.SalesPersonName = "";
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(defaultSalesPersonId))
                            {
                                DataTable dtSalesManName = odac.getDefaultSalesPersonName(defaultSalesPersonId);
                                if (dtSalesManName.Rows.Count > 0)
                                {
                                    defaultSalesPersonName = dtSalesManName.Rows[0][0].ToString();
                                }
                                if (!string.IsNullOrEmpty(defaultSalesPersonName))
                                {
                                    foreach (SaleLineItem item in retailTransaction.SaleItems)
                                    {
                                        if (string.IsNullOrEmpty(item.SalesPersonId) && string.IsNullOrEmpty(item.SalespersonName))
                                        {
                                            item.SalesPersonId = defaultSalesPersonId;
                                            item.SalespersonName = defaultSalesPersonName;
                                        }
                                    }
                                    if (string.IsNullOrEmpty(retailTransaction.SalesPersonId))
                                    {
                                        retailTransaction.SalesPersonId = defaultSalesPersonId;
                                        retailTransaction.SalesPersonName = defaultSalesPersonName;
                                    }
                                    retailTransaction.Save();

                                    //Validación de tiendas que verifican el vendedor
                                    foreach (SaleLineItem item in retailTransaction.SaleItems)
                                    {
                                        if (verificaVendedor == 1)
                                        {
                                            if (!string.IsNullOrEmpty(defaultSalesPersonId))
                                            {
                                                if (item.SalesPersonId == defaultSalesPersonId)
                                                {
                                                    Application.Services.Dialog.ShowMessage("La tienda tiene configurado un vendedor por default: " + defaultSalesPersonId + ", el cual está asignado a una linea de productos en la venta, favor de seleccionar un vendedor diferente.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                                    AsignaNuevoVendedor(retailTransaction, defaultSalesPersonId);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Application.Services.Dialog.ShowMessage("Error al cargar el vendedor default. Contacte con sistemas. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion

            #region GRW Impresión de ticket
            DAC oDac = new DAC(ApplicationSettings.Database.LocalConnection);
            if (posTransaction is RetailTransaction)
            {
                try
                {
                    RetailTransaction retailTransaction = posTransaction as RetailTransaction;

                    if (retailTransaction.Comment.Trim() == "suspendetransaccion" && retailTransaction.EntryStatus == PosTransaction.TransactionStatus.OnHold)
                    {
                        string custid;
                        if (retailTransaction.Customer.CustomerId != null)
                        {
                            custid = retailTransaction.Customer.CustomerId;
                        }
                        else
                        {
                            custid = oDac.getDeftCust(ApplicationSettings.Terminal.StoreId);
                        }
                        string suspendedtran = Application.BusinessLogic.SuspendRetrieveSystem.GetLastSuspendedTransactionId(Application.Shift.StoreId, Application.Shift.TerminalId);
                        if (!oDac.insertSuspendedTran(retailTransaction.StoreId, retailTransaction.TerminalId, retailTransaction.TransactionId, suspendedtran, retailTransaction.ChannelId, defaultSalesPersonId, defaultSalesPersonName))
                        {
                            Application.Services.Dialog.ShowMessage("Error al guardar transacción.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        reportLayout = new StringBuilder(2500);
                        reportLayout.AppendLine(singleLine);
                        reportLayout.AppendLine("                    TICKET DE VENTA");
                        reportLayout.AppendLine(singleLine);
                        reportLayout.AppendLine("        Fecha: " + DateTime.Now.ToShortDateString() + "       Importe: " + Application.Services.Rounding.RoundForDisplay(retailTransaction.AmountDue, true, true));
                        reportLayout.AppendLine();
                        reportLayout.AppendLine("Transacción: " + retailTransaction.TransactionId);
                        reportLayout.AppendLine("Cliente: " + custid);
                        reportLayout.AppendLine();
                        reportLayout.AppendLine("<B: " + retailTransaction.TransactionId + ">");
                        reportLayout.AppendLine();
                        reportLayout.AppendLine(Convert.ToChar(27) + "|2C      ESTE TICKET NO ES UN COMPROBANTE DE PAGO" + Convert.ToChar(27) + "|1C");
                        reportLayout.AppendLine();
                        reportLayout.AppendLine();
                        reportLayout.AppendLine();
                        reportLayout.AppendLine();

                        //for (int i = 0; i < 2; i++)
                        //{
                            Application.Services.Peripherals.Printer.PrintReceipt(reportLayout.ToString());
                        //}
                    }
                }
                catch (Exception ex)
                {
                    Application.Services.Dialog.ShowMessage(ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            #endregion
        }

        private void AsignaNuevoVendedor(RetailTransaction retTran, string salesPersonId)
        {
            try
            {
                using (LSRetailPosis.POSProcesses.frmSalesPerson dialog = new LSRetailPosis.POSProcesses.frmSalesPerson())
                {
                    LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
                    if (dialog.DialogResult == DialogResult.OK)
                    {
                        employeeid = dialog.SelectedEmployeeId;
                        employeename = dialog.SelectEmployeeName;
                    }
                    else
                    {
                        Application.Services.Dialog.ShowMessage("Debe agregar para esta tienda un vendedor distinto al vendedor default", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                if (salesPersonId == employeeid)
                {
                    Application.Services.Dialog.ShowMessage("La tienda tiene configurado un vendedor por default: " + salesPersonId, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    DialogResult dialogResult = Application.Services.Dialog.ShowMessage("¿Desea dejar en la transacción el vendedor por default?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.No)
                    {
                        AsignaNuevoVendedor(retTran, salesPersonId);
                    }
                    else if (dialogResult == DialogResult.Yes)
                    {
                        retTran.Save();
                    }
                    else
                    {
                        AsignaNuevoVendedor(retTran, salesPersonId);
                    }
                }
                else
                {
                    retTran.SalesPersonId = employeeid;
                    retTran.SalesPersonName = employeename;
                    if (retTran.SaleItems.Count > 0)
                    {
                        foreach (SaleLineItem oldSaleitem in retTran.SaleItems)
                        {
                            if (!oldSaleitem.Voided)
                            {
                                if (string.IsNullOrEmpty(oldSaleitem.SalesPersonId))
                                {
                                    oldSaleitem.SalesPersonId = employeeid;
                                    oldSaleitem.SalespersonName = employeename;

                                    DAC oDac = new DAC(Application.Settings.Database.Connection);
                                    oDac.updateSuspendedTranDetail(retTran.StoreId, retTran.TerminalId, retTran.TransactionId, employeeid, employeename, oldSaleitem.LineId, retTran.ChannelId);
                                }
                                else
                                {
                                    if (oldSaleitem.SalesPersonId == salesPersonId)
                                    {
                                        oldSaleitem.SalesPersonId = employeeid;
                                        oldSaleitem.SalespersonName = employeename;
                                        DAC oDac = new DAC(Application.Settings.Database.Connection);
                                        oDac.updateSuspendedTranDetail(retTran.StoreId, retTran.TerminalId, retTran.TransactionId, employeeid, employeename, oldSaleitem.LineId, retTran.ChannelId);
                                    }
                                }
                            }
                        }
                    }
                    retTran.Save();
                }
            }
            catch (Exception ex)
            {
                Application.Services.Dialog.ShowMessage("Error al cargar el vendedor. Contacte con sistemas. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void PostEndTransaction(IPosTransaction posTransaction)
        {
            #region GRW Insert CARDORACCOUNT field
            try
            {
                RetailTransaction retTran = posTransaction as RetailTransaction;
                if (posTransaction is RetailTransaction || posTransaction is CustomerOrderTransaction)
                {
                    if (retTran.EntryStatus != PosTransaction.TransactionStatus.Voided)
                    {
                        if (retTran.NoOfPaymentLines > 0)
                        {
                            foreach (TenderLineItem tender in retTran.TenderLines)
                            {
                                if (!tender.Voided && (tender.Comment == "Pago MIT" || tender.Comment == "Pago AppliNET"))
                                {
                                    odac = new DAC(Application.Settings.Database.Connection);
                                    if (!odac.insertaCARDORACCOUNT(tender.LineId, retTran.StoreId, retTran.Shift.TerminalId, retTran.TransactionId, Application.Settings.Database.DataAreaID))
                                    {
                                        Application.Services.Dialog.ShowMessage("No se ha podido actualizar la tabla PaymentTrans con la información de CardOrAccount, favor de notificar a sistemas. El proceso continuará.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Application.Services.Dialog.ShowMessage("Error al agregar información al campo CARDORACCOUNT. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            #endregion

            #region GRW Insert INSERTCARDTYPEID
            try
            {
                RetailTransaction retTran = posTransaction as RetailTransaction;
                if (posTransaction is RetailTransaction || posTransaction is CustomerOrderTransaction)
                {
                    if (retTran.EntryStatus != PosTransaction.TransactionStatus.Voided)
                    {
                        if (retTran.NoOfPaymentLines > 0)
                        {
                            foreach (TenderLineItem tender in retTran.TenderLines)
                            {
                                if (!tender.Voided && (tender.Comment == "Pago MIT" || tender.Comment == "Pago AppliNET"))
                                {
                                    odac = new DAC(Application.Settings.Database.Connection);
                                    if (!odac.InsertCardTypeId(tender.LineId, retTran.StoreId, retTran.Shift.TerminalId, retTran.TransactionId, Application.Settings.Database.DataAreaID))
                                    {
                                        Application.Services.Dialog.ShowMessage("No se ha podido actualizar la tabla PaymentTrans con la información de CARDTYPEID, favor de notificar a sistemas. El proceso continuará.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Application.Services.Dialog.ShowMessage("Error al agregar información al campo CARDTYPEID. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            #endregion

            #region Manda Ticket sin datos del cliente
            if (posTransaction is RetailTransaction)
            {

                RetailTransaction retailTransaction = posTransaction as RetailTransaction;
                if ((retailTransaction.EntryStatus == PosTransaction.TransactionStatus.Normal) && Math.Abs(retailTransaction.Payment) > 0 && retailTransaction.Payment == retailTransaction.AmountDue)
                {
                    string param = "cfdipos;";
                    param += retailTransaction.StoreId + "|";
                    param += retailTransaction.TerminalId + "|";
                    param += retailTransaction.TransactionId + "|";
                    if (retailTransaction.SaleIsReturnSale)
                    {
                        string returnTrans = string.Empty;
                        foreach (ISaleLineItem saleLine in retailTransaction.SaleItems)
                        {
                            if (saleLine.ReturnTransId != string.Empty)
                            {
                                returnTrans = saleLine.ReturnTransId;
                                break;
                            }
                        }
                        DAC odac = new DAC(Application.Settings.Database.Connection);
                        string receiptId = odac.getReturnReceipt(Application.Settings.Database.Connection, retailTransaction.StoreId, retailTransaction.TerminalId, returnTrans);
                        param += "true|" + receiptId;
                    }
                    else
                        param += "false|0";
                    Application.RunOperation(PosisOperations.BlankOperation, param);
                }
            }
            #endregion
        }

        public void PreVoidTransaction(IPreTriggerResult preTriggerResult, IPosTransaction posTransaction)
        {
            LSRetailPosis.ApplicationLog.Log("TransactionTriggers.PreVoidTransaction", "Before voiding the transaction...", LSRetailPosis.LogTraceLevel.Trace);
        }

        public void PostVoidTransaction(IPosTransaction posTransaction)
        {
            string source = "TransactionTriggers.PostVoidTransaction";
            string value = "After voiding the transaction...";
            LSRetailPosis.ApplicationLog.Log(source, value, LSRetailPosis.LogTraceLevel.Trace);
            LSRetailPosis.ApplicationLog.WriteAuditEntry(source, value);
        }

        public void PreReturnTransaction(IPreTriggerResult preTriggerResult, IRetailTransaction originalTransaction, IPosTransaction posTransaction)
        {
            LSRetailPosis.ApplicationLog.Log("TransactionTriggers.PreReturnTransaction", "Before returning the transaction...", LSRetailPosis.LogTraceLevel.Trace);
        }

        public void PostReturnTransaction(IPosTransaction posTransaction)
        {
            LSRetailPosis.ApplicationLog.Log("TransactionTriggers.PostReturnTransaction", "After returning the transaction...", LSRetailPosis.LogTraceLevel.Trace);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1")]
        public void PreConfirmReturnTransaction(IPreTriggerResult preTriggerResult, IRetailTransaction originalTransaction)
        {
            LSRetailPosis.ApplicationLog.Log("TransactionTriggers.PreConfirmReturnTransaction", "Before confirming return transaction...", LogTraceLevel.Trace);
        }
    }
}
