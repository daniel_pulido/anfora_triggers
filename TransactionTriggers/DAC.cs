﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using LSRetailPosis.Settings;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using System.Collections;

namespace Microsoft.Dynamics.Retail.Pos.TransactionTriggers
{
    class DAC
    {
        private SqlConnection conn;
        public DAC(SqlConnection connData)
        {
            conn = connData;
        }

        public DataTable selectData(string consulta)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = consulta;
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public bool insertaCARDORACCOUNT(int lineId, string storeId, string terminalId, string transactionId, string dataAreaId)
        {
            try
            {
                DataTable DtResults = new DataTable();
                string cmdText = "EXEC xsp_GRW_INSERTCARDORACCOUNT @DataAreaId, @StoreId, @Terminal, @TransactionId, @LineId";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@StoreId", SqlDbType.NVarChar, 10).Value = storeId;
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 4).Value = dataAreaId;
                    command.Parameters.Add("@Terminal", SqlDbType.NVarChar, 10).Value = terminalId;
                    command.Parameters.Add("TransactionId", SqlDbType.NVarChar, 44).Value = transactionId;
                    command.Parameters.Add("@LineId", SqlDbType.Int).Value = lineId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return true;
        }

        public bool InsertCardTypeId(int lineId, string storeId, string terminalId, string transactionId, string dataAreaId)
        {
            try
            {
                DataTable DtResults = new DataTable();
                string cmdText = "EXEC xsp_GRW_INSERTCARDTYPEID @DataAreaId, @StoreId, @Terminal, @TransactionId, @LineId";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@StoreId", SqlDbType.NVarChar, 10).Value = storeId;
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 4).Value = dataAreaId;
                    command.Parameters.Add("@Terminal", SqlDbType.NVarChar, 10).Value = terminalId;
                    command.Parameters.Add("TransactionId", SqlDbType.NVarChar, 44).Value = transactionId;
                    command.Parameters.Add("@LineId", SqlDbType.Int).Value = lineId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return true;
        }

        public string getReturnReceipt(SqlConnection _sqlConn, string _store, string _terminal, string _transId)
        {
            string retVal = string.Empty;
            DataTable DtResults = new DataTable();
            try
            {
                if (_sqlConn.State != ConnectionState.Open)
                {
                    _sqlConn.Open();
                }
                string commText = "exec xsp_getReturnReceipt @store, @terminal, @transactionid";
                SqlCommand sqlComm = new SqlCommand(commText, _sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@store", (object)_store));
                sqlComm.Parameters.Add(new SqlParameter("@terminal", (object)_terminal));
                sqlComm.Parameters.Add(new SqlParameter("@transactionid", (object)_transId));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
                if (DtResults.Rows.Count > 0)
                {
                    retVal = DtResults.Rows[0]["ReceiptId"].ToString();
                }
            }
            finally
            {
                _sqlConn.Close();
            }
            return retVal;
        }

        public DataTable getDefaultSalesPerson(string storeId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "SELECT [GRWDEFAULTSALESMAN],[GRWASKSALESPERSON] FROM [ax].[RETAILSTORETABLE] WHERE STORENUMBER = @STOREID ";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@STOREID", SqlDbType.NVarChar, 10).Value = storeId;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public DataTable getDefaultSalesPersonName(string StaffId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "SELECT [NAMEONRECEIPT] FROM [ax].[RETAILSTAFFTABLE] where [STAFFID] = @STAFF";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@STAFF", SqlDbType.NVarChar, 25).Value = StaffId;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public string getDeftCust(string storeId)
        {
            string str = null;
            string cmdText = "select c.* from RETAILCHANNELTABLE c inner join RETAILSTORETABLE s on c.RECID = s.RECID where s.Storenumber =  @STOREID ";
            try
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@STOREID", SqlDbType.NVarChar, 10).Value = storeId;
                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            str = reader["DefaultCustAccount"] as string;
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return str;
        }

        public bool insertSuspendedTran(string storeId, string terminalid, string transactionId, string transactionidR3, Int64 channelId, string salesPersonId, string salesPersonName)
        {
            try
            {
                string cmdText = "INSERT INTO [dbo].[GRWSUSPENDEDTRAN]([TRANSACTIONID],[TERMINALID],[STORE],[SUSPENDEDTRAN],[CHANNELID],[STATUS],[SALESPERSONID],[SALESPERSONNAME])VALUES(@TRANSACTIONID,@TERMINALID,@STORE,@SUSPENDEDTRAN,@CHANNELID,1, @SALESPERSONID, @SALESPERSONNAME)";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@TRANSACTIONID", SqlDbType.NVarChar, 44).Value = transactionId;
                    command.Parameters.Add("@TERMINALID", SqlDbType.NVarChar, 10).Value = terminalid;
                    command.Parameters.Add("@STORE", SqlDbType.NVarChar, 10).Value = storeId;
                    command.Parameters.Add("@SUSPENDEDTRAN", SqlDbType.NVarChar, 44).Value = transactionidR3;
                    command.Parameters.Add("@CHANNELID", SqlDbType.BigInt).Value = channelId;
                    command.Parameters.Add("@SALESPERSONID", SqlDbType.NVarChar, 100).Value = salesPersonId;
                    command.Parameters.Add("@SALESPERSONNAME", SqlDbType.NVarChar, 200).Value = salesPersonName;
                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return true;
        }

        public bool updateSuspendedTranDetail(string storeId, string terminalid, string transactionId, string salesPersonId, string salesPersonName, Int64 lineNumber, Int64 channelId)
        {
            try
            {
                string cmdText = "EXEC xsp_UpdateSalesPerson @TRANSACTIONID,@TERMINALID,@STORE, @CHANNELID, @SALESPERSONID, @SALESPERSONNAME, @LINENUM";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@TRANSACTIONID", SqlDbType.NVarChar, 44).Value = transactionId;
                    command.Parameters.Add("@TERMINALID", SqlDbType.NVarChar, 10).Value = terminalid;
                    command.Parameters.Add("@STORE", SqlDbType.NVarChar, 10).Value = storeId;
                    command.Parameters.Add("@CHANNELID", SqlDbType.BigInt).Value = channelId;
                    command.Parameters.Add("@SALESPERSONID", SqlDbType.NVarChar, 100).Value = salesPersonId;
                    command.Parameters.Add("@SALESPERSONNAME", SqlDbType.NVarChar, 200).Value = salesPersonName;
                    command.Parameters.Add("@LINENUM", SqlDbType.BigInt).Value = lineNumber;
                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return true;
        }
    }
}
