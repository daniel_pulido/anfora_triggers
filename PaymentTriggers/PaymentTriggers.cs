/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

//Microsoft Dynamics AX for Retail POS Plug-ins 
//The following project is provided as SAMPLE code. Because this software is "as is," we may not provide support services for it.

using System.ComponentModel.Composition;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using Microsoft.Dynamics.Retail.Pos.Contracts.Triggers;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.Transaction.Line.TenderItem;
using LSRetailPosis.Transaction;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using System.Windows.Forms;
using cpIntegracionEMV;
using System.Data;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Dynamics.Retail.Notification.Contracts;
using System;
using System.Data.SqlClient;
using System.Xml;
using LSRetailPosis.Settings;

namespace Microsoft.Dynamics.Retail.Pos.PaymentTriggers
{
    [Export(typeof(IPaymentTrigger))]
    public class PaymentTriggers : IPaymentTrigger
    {
        [Import]
        public IApplication Application { get; set; }

        #region MIT
        private RetailTransaction Transaction;
        private DAC odac;
        TenderLineItem tlGlobal;
        public string tendertype;
        private ConfigClass configClass;
        public static cpIntegracionEMV.clsCpIntegracionEMV cpIntegraEMV = new clsCpIntegracionEMV();
        public static cpIntegracionEMV.clsPrePagoTrx PPOperacion = new clsPrePagoTrx();
        public static cpIntegracionEMV.clsServicios Servicios = new clsServicios();
        private string referencia;
        private string Url;
        private string respuestaMerchant;
        private string OperationNumber;
        private string AutNumber;
        string formaPagoCredito = string.Empty;
        string formaPagoDebito = string.Empty;
        string formaPagoAmex = string.Empty;
        //Returned transactions
        private string VComercio;
        private string VCliente;
        private bool formatedXML;
        private bool LogueoUsuario = false;
        private bool? TransaccionCancelada = null;
        private int status;
        Timer timer01 = new Timer();
        private int tiempoTimer;
        Timer timer02 = new Timer();
        private int tiempoTimer2;
        private string tenderTypeId;
        private string cardTypeId;
        private DataTable DtFormas;
        #endregion

        #region Constructor - Destructor

        public PaymentTriggers()
        {
            // Get all text through the Translation function in the ApplicationLocalizer
            // TextID's for PaymentTriggers are reserved at 50400 - 50449
        }

        #endregion

        #region IPaymentTriggers Members

        public void PrePayCustomerAccount(IPreTriggerResult preTriggerResult, IPosTransaction posTransaction, decimal amount)
        {
            LSRetailPosis.ApplicationLog.Log("PaymentTriggers.PrePayCustomerAccount", "Before charging to a customer account", LSRetailPosis.LogTraceLevel.Trace);
        }

        public void PrePayCardAuthorization(IPreTriggerResult preTriggerResult, IPosTransaction posTransaction, ICardInfo cardInfo, decimal amount)
        {
            LSRetailPosis.ApplicationLog.Log("PaymentTriggers.PrePayCardAuthorization", "Before the EFT authorization", LSRetailPosis.LogTraceLevel.Trace);
        }

        /// <summary>
        /// <example><code>
        /// // In order to delete the already-added payment you use the following code:
        /// if (retailTransaction.TenderLines.Count > 0)
        /// {
        ///     retailTransaction.TenderLines.RemoveLast();
        ///     retailTransaction.LastRunOpererationIsValidPayment = false;
        /// }
        /// </code></example>
        /// </summary>
        public void OnPayment(IPosTransaction posTransaction)
        {
            LSRetailPosis.ApplicationLog.Log("PaymentTriggers.OnPayment", "On the addition of a tender...", LSRetailPosis.LogTraceLevel.Trace);
        }

        public void PrePayment(IPreTriggerResult preTriggerResult, IPosTransaction posTransaction, object posOperation, string tenderId)
        {
            LSRetailPosis.ApplicationLog.Log("PaymentTriggers.PrePayment", "On the start of a payment operation...", LSRetailPosis.LogTraceLevel.Trace);

            switch ((PosisOperations)posOperation)
            {
                case PosisOperations.PayCash:
                    // Insert code here...
                    break;
                case PosisOperations.PayCard:
                    // Insert code here...
                    break;
                case PosisOperations.PayCheque:
                    // Insert code here...
                    break;
                case PosisOperations.PayCorporateCard:
                    // Insert code here...
                    break;
                case PosisOperations.PayCreditMemo:
                    // Insert code here...
                    break;
                case PosisOperations.PayCurrency:
                    // Insert code here...
                    break;
                case PosisOperations.PayCustomerAccount:
                    // Insert code here...
                    break;
                case PosisOperations.PayGiftCertificate:
                    // Insert code here...
                    break;
                case PosisOperations.PayLoyalty:
                    // Insert code here...
                    break;

                // etc.....
            }
        }

        /// <summary>
        /// Triggered before voiding of a payment.
        /// </summary>
        /// <param name="preTriggerResult"></param>
        /// <param name="posTransaction"></param>
        /// <param name="lineId"> </param>
        public void PreVoidPayment(IPreTriggerResult preTriggerResult, IPosTransaction posTransaction, int lineId)
        {
            LSRetailPosis.ApplicationLog.Log("PaymentTriggers.PreVoidPayment", "Before the void payment operation...", LSRetailPosis.LogTraceLevel.Trace);

            #region MIT
            RetailTransaction retailTransaction = null;
            if (posTransaction is RetailTransaction)
            {
                retailTransaction = posTransaction as RetailTransaction;
                if (retailTransaction.SaleItems.Count > 0 && retailTransaction.AmountDue > 0)
                {
                    if (retailTransaction.TenderLines.Count > 0)
                    {
                        foreach (TenderLineItem tl in retailTransaction.TenderLines)
                        {
                            if (!tl.Voided)
                            {
                                if (tl.Comment == "Pago MIT" && tl.LineId == lineId)
                                {
                                    cargaFormasPago();
                                    tlGlobal = tl;
                                    Transaction = retailTransaction;
                                    odac = new DAC(Application.Settings.Database.Connection);
                                    try
                                    {
                                        //ManagerAccessConfirmation managerAccessInteraction = new ManagerAccessConfirmation();
                                        //InteractionRequestedEventArgs request = new InteractionRequestedEventArgs(managerAccessInteraction, () => { });
                                        //Application.Services.Interaction.InteractionRequest(request);
                                        //if (managerAccessInteraction.Confirmed)
                                        //{
                                        status = 0;
                                        InitConfigParamAdmin(retailTransaction.StoreId, retailTransaction.TerminalId);
                                        LogUsuario();
                                        if (LogueoUsuario)
                                        {
                                            CargaCancelacion(tl, retailTransaction);
                                            if (!(bool)TransaccionCancelada)
                                            {
                                                preTriggerResult.ContinueOperation = false;
                                            }
                                            if (!string.IsNullOrEmpty(OperationNumber) &&
                                                !string.IsNullOrEmpty(AutNumber) &&
                                                 !string.IsNullOrEmpty(referencia))
                                            {
                                                EjecutaCancelacion(tl, retailTransaction);
                                                //using (procesando frmProcesando = new procesando())
                                                //{
                                                //    LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(frmProcesando);
                                                //    System.Threading.Thread.Sleep(1000);
                                                //    frmProcesando.Close();
                                                //}
                                                if (cpIntegraEMV.getRspDsResponse() != "")
                                                {
                                                    timer01.Enabled = false;
                                                    formatedXML = false;
                                                    VerificaRespuestaCancelacion();
                                                    if (TransaccionCancelada != null)
                                                    {
                                                        if (!(bool)TransaccionCancelada)
                                                        {
                                                            Application.Services.Dialog.ShowMessage("No se puede cancelar el pago con MIT, favor de contactar con sistemas y concluya la transacci�n actual.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                            preTriggerResult.ContinueOperation = false;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    tiempoTimer = 0;
                                                    timer01.Interval = 3000;
                                                    timer01.Enabled = true;
                                                    timer01.Tick += new EventHandler(timerprocCancela);
                                                    timer01.Start();
                                                }
                                                if (TransaccionCancelada != null)
                                                {
                                                    if (!(bool)TransaccionCancelada)
                                                    {
                                                        Application.Services.Dialog.ShowMessage("No se puede cancelar el pago con MIT, favor de contactar con sistemas y concluya la transacci�n actual.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                        preTriggerResult.ContinueOperation = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        //}
                                        //else
                                        //{
                                        //    Application.Services.Dialog.ShowMessage("La operaci�n requiere privilegios de supervisor", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        //    TransaccionCancelada = false;
                                        //    CancelarOperacion();
                                        //}
                                    }
                                    catch (Exception ex)
                                    {
                                        TransaccionCancelada = false;
                                        Application.Services.Dialog.ShowMessage(ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        preTriggerResult.ContinueOperation = false;
                                        break;
                                    }
                                }
                                else if (tl.Comment == "Pago AppliNET" && tl.LineId == lineId)
                                {
                                    Application.Services.Dialog.ShowMessage("Para cancelar un pago con AppliNET por favor finalice la actual transacci�n e inmediatamente realice una cancelaci�n/devoluci�n con Pago AppliNET.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    preTriggerResult.ContinueOperation = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (retailTransaction.SaleItems.Count > 0 && retailTransaction.AmountDue < 0)
                {
                    if (retailTransaction.TenderLines.Count > 0)
                    {
                        foreach (TenderLineItem tl in retailTransaction.TenderLines)
                        {
                            if (!tl.Voided)
                            {
                                if (tl.Comment == "Pago MIT" && tl.LineId == lineId)
                                {
                                    Application.Services.Dialog.ShowMessage("Ya se ha cancelado un pago con tarjeta bancaria, favor de culminar la transacci�n.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    preTriggerResult.ContinueOperation = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            #endregion
        }

        /// <summary>
        /// Triggered after voiding of a payment.
        /// </summary>
        /// <param name="posTransaction"></param>
        /// <param name="lineId"> </param>
        public void PostVoidPayment(IPosTransaction posTransaction, int lineId)
        {
            LSRetailPosis.ApplicationLog.Log("PaymentTriggers.PostVoidPayment", "After the void payment operation...", LSRetailPosis.LogTraceLevel.Trace);
        }

        /// <summary>
        /// Triggered before registering cash payment.
        /// </summary>
        /// <param name="preTriggerResult"></param>
        /// <param name="posTransaction"></param>
        /// <param name="posOperation"></param>
        /// <param name="tenderId"></param>
        /// <param name="currencyCode"></param>
        /// <param name="amount"></param>
        public void PreRegisterPayment(IPreTriggerResult preTriggerResult, IPosTransaction posTransaction, object posOperation, string tenderId, string currencyCode, decimal amount)
        {
            LSRetailPosis.ApplicationLog.Log("PaymentTriggers.PreRegisterPayment", "Before registering the payment...", LSRetailPosis.LogTraceLevel.Trace);
        }

        #endregion

        #region M�todos MIT

        //private void CancelacionPagoMIT(TenderLineItem tl, RetailTransaction retTran)
        //{
        //    try
        //    {
        //        ManagerAccessConfirmation managerAccessInteraction = new ManagerAccessConfirmation();
        //        InteractionRequestedEventArgs request = new InteractionRequestedEventArgs(managerAccessInteraction, () => { });
        //        Application.Services.Interaction.InteractionRequest(request);
        //        if (managerAccessInteraction.Confirmed)
        //        {
        //            status = 0;
        //            InitConfigParamAdmin(retTran.StoreId);
        //            LogUsuario();
        //            if (LogueoUsuario)
        //            {
        //                CargaCancelacion(tl, retTran);

        //                if (!string.IsNullOrEmpty(OperationNumber) &&
        //                    !string.IsNullOrEmpty(AutNumber) &&
        //                     !string.IsNullOrEmpty(referencia))
        //                {
        //                    EjecutaCancelacion(tl, retTran);
        //                    tiempoTimer = 0;
        //                    timer01.Interval = 3000;
        //                    timer01.Enabled = true;
        //                    timer01.Tick += new EventHandler(timerprocCancela);
        //                    timer01.Start();
        //                }
        //            }
        //        }
        //        else
        //        {
        //            Application.Services.Dialog.ShowMessage("La operaci�n requiere privilegios de supervisor", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            TransaccionCancelada = false;
        //            CancelarOperacion();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        TransaccionCancelada = false;
        //        Application.Services.Dialog.ShowMessage(ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        private void CancelarOperacion()
        {
            cpIntegraEMV.dbgCancelOperation();
        }

        public void InitConfigParamAdmin(string StoreId, string TerminalId)
        {
            configClass = new ConfigClass();
            try
            {
                DAC odac = new DAC(new SqlConnection(Application.Settings.Database.Connection.ConnectionString));
                DataTable DtConfig = odac.getMITConfigAdmin(StoreId, TerminalId);
                configClass = new ConfigClass();
                if (DtConfig.Rows.Count > 0)
                {
                    configClass.User = DtConfig.Rows[0]["Bs_User"].ToString();
                    configClass.Pass = RijndaelDecrypt.Decrypt(DtConfig.Rows[0]["Bs_Pwd"].ToString());
                    configClass.Url = DtConfig.Rows[0]["UrlMIT"].ToString();
                    configClass.Currency = DtConfig.Rows[0]["Tx_Currency"].ToString();
                    configClass.Tp_operacion = DtConfig.Rows[0]["Tp_operacion"].ToString();
                    configClass.TimerOut = DtConfig.Rows[0]["TimerOut"].ToString();
                    configClass.TimerResp = int.Parse(DtConfig.Rows[0]["TimerResp"].ToString());

                    Url = configClass.Url;
                    cpIntegraEMV.dbgSetUrl(ref Url);
                    cpIntegraEMV.dbgSetCurrencyType(configClass.Currency);
                }
                else
                {
                    Application.Services.Dialog.ShowMessage("No se pudo cargar la configuraci�n de la Terminal, favor de contactar con Sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    TransaccionCancelada = false;
                }
            }
            catch (Exception ex)
            {
                TransaccionCancelada = false;
                Application.Services.Dialog.ShowMessage("No se pudo cargar la configuraci�n de la Terminal, favor de contactar con Sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Services.Dialog.ShowMessage("Descripci�n del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                CancelarOperacion();
            }
        }

        private void LogUsuario()
        {
            try
            {
                if (cpIntegraEMV.dbgLoginUser(configClass.User, configClass.Pass))
                {
                    CargaObjetos();
                    LogueoUsuario = true;
                }
                else
                {
                    string respuesta =
                       "               Error \n" +
                       "Error de autenticaci�n de usuario \n" +
                       "Verifique la instalaci�n de la TPV \n" +
                       cpIntegraEMV.dbgGetRspError();
                    Application.Services.Dialog.ShowMessage(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    TransaccionCancelada = false;
                }
            }
            catch
            {
                TransaccionCancelada = false;
                CancelarOperacion();
            }
        }

        private void CargaObjetos()
        {
            configClass.User = cpIntegraEMV.dbgGetUser();
            configClass.Pass = cpIntegraEMV.dbgGetPass();
            configClass.Company = cpIntegraEMV.dbgGetId_Company();
            configClass.Branch = cpIntegraEMV.dbgGetId_Branch();
            configClass.Country = cpIntegraEMV.dbgGetCountry();
            configClass.Nb_User = cpIntegraEMV.dbgGetNb_User();
            configClass.Nb_Company = cpIntegraEMV.dbgGetNb_Company();
            configClass.Nb_companystreet = cpIntegraEMV.dbgGetNb_companystreet();
            configClass.Nb_Branch = cpIntegraEMV.dbgGetNb_Branch();

            configClass.ChkReverso = false;
            if (cpIntegraEMV.dbgGetActivateReverse() == "0")
            {
                configClass.ChkReverso = false;
            }
            else
            {
                configClass.ChkReverso = true;
            }
            //tiempo que tiene el usuario para deslizar o insertar la tarjeta antes de que la terminal vuelva a su estado de reposo.
            cpIntegraEMV.dbgSetTimeOut(configClass.TimerOut);

            configClass.pagomVMC = cpIntegraEMV.dbgGetpagomVMC();
            configClass.pagomAMEX = cpIntegraEMV.dbgGetpagomAMEX();
            configClass.pagobVMC = cpIntegraEMV.dbgGetpagobVMC();
            configClass.pagobAMEX = cpIntegraEMV.dbgGetpagobAMEX();
            configClass.pagobSIP = cpIntegraEMV.dbgGetpagobSIP();
            configClass.FacturaE = cpIntegraEMV.dbgGetFacturaE();
            configClass.Points2 = cpIntegraEMV.dbgGetPoints2();
        }

        private void CargaCancelacion(TenderLineItem tl, RetailTransaction retTran)
        {
            try
            {
                AutNumber = string.Empty;
                OperationNumber = string.Empty;
                referencia = string.Empty;

                DAC odac = new DAC(new SqlConnection(Application.Settings.Database.Connection.ConnectionString));
                DataTable DtConfig = odac.selectData("Select * from [ax].[GRW_MITTRAN] where DataAreaId = '" + Application.Settings.Database.DataAreaID + "' AND TRANSACTIONID = '" + retTran.TransactionId + "' AND LINEID = '" + tl.LineId + "' AND STOREID = '" + retTran.StoreId + "' AND TERMINAL = '" + retTran.Shift.TerminalId + "'");
                if (DtConfig.Rows.Count > 0)
                {
                    OperationNumber = DtConfig.Rows[0]["TX_OPERATIONNUMBER"].ToString();
                    AutNumber = DtConfig.Rows[0]["TX_AUTH"].ToString();
                    referencia = DtConfig.Rows[0]["TX_REFERENCE"].ToString();
                }
                else
                {
                    Application.Services.Dialog.ShowMessage("No se pudo cargar la transacci�n de cancelaci�n, favor de contactar con Sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    CancelarOperacion();
                    TransaccionCancelada = false;
                }
            }
            catch (Exception ex)
            {
                Application.Services.Dialog.ShowMessage("No se pudo cargar la transacci�n de cancelaci�n.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Services.Dialog.ShowMessage("Descripci�n del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                CancelarOperacion();
                TransaccionCancelada = false;
            }
        }

        private void EjecutaCancelacion(TenderLineItem tl, RetailTransaction retTran)
        {
            string amountCancela = tl.Amount.ToString();
            cpIntegraEMV.sndCancelacion(configClass.User, configClass.Pass, retTran.Shift.StaffId, configClass.Company, configClass.Branch, configClass.Country, amountCancela, OperationNumber, AutNumber);
        }

        private void timerprocCancela(object o1, EventArgs e1)
        {
            if (tiempoTimer < configClass.TimerResp)
            {
                if (cpIntegraEMV.getRspDsResponse() != "")
                {
                    timer01.Enabled = false;
                    formatedXML = false;
                    VerificaRespuestaCancelacion();
                }
                else
                {
                    tiempoTimer += timer01.Interval;
                }
            }
            else
            {
                formatedXML = true;
                timer01.Enabled = false;
                string retVal = string.Empty;
                retVal = cpIntegraEMV.sndConsulta(configClass.User, configClass.Pass, configClass.Company, configClass.Branch, DateTime.Today.ToString("dd/MM/yyyy"), referencia);
                if (!string.IsNullOrEmpty(retVal))
                {
                    retVal = retVal.Substring(retVal.IndexOf('<'), retVal.IndexOf("</transacciones>") + 16);
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(retVal);
                    XmlNode responseVenta = xmlDoc.DocumentElement.SelectSingleNode("transaccion/nb_response");
                    if (!string.IsNullOrEmpty(responseVenta.InnerText))
                    {
                        VerificaRespuestaCancelacionXML(xmlDoc);
                    }
                    else
                    {
                        TransaccionCancelada = false;
                        cpIntegraEMV.dbgCancelOperation();
                        Application.Services.Dialog.ShowMessage("No hay respuesta del emisor intente m�s tarde", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        CancelarOperacion();
                    }
                }
                else
                {
                    TransaccionCancelada = false;
                    cpIntegraEMV.dbgCancelOperation();
                    Application.Services.Dialog.ShowMessage("No hay respuesta del emisor intente m�s tarde", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                }
            }
        }

        private void VerificaRespuestaCancelacion()
        {
            string respuesta = string.Empty;
            string txtVoucher = string.Empty;
            switch (cpIntegraEMV.getRspDsResponse())
            {
                case "approved":  //Transacci�n Aprobada
                    TransaccionCancelada = true;
                    respuesta = "      Cancelaci�n aprobada" + "\n" +
                    "N�m. de Referencia: " + cpIntegraEMV.getTx_Reference() + "\n" +
                    "N�m. de Operaci�n: " + cpIntegraEMV.getRspOperationNumber() + "\n" +
                    "N�m. de Autorizaci�n: " + cpIntegraEMV.getRspAuth() + "\n" +
                    "Tipo de Tarjeta: " + cpIntegraEMV.getCc_Type() + "\n" +
                    "Nombre TH: " + cpIntegraEMV.getCc_Name() + "\n" +
                    "N�m. Tarjeta: " + cpIntegraEMV.getCc_Number() + "\n" +
                    "Vencimiento: " + cpIntegraEMV.getCc_ExpMonth() + "/" + cpIntegraEMV.getCc_ExpYear() + "\n" +
                    "Monto Cancelaci�n: $" + cpIntegraEMV.getTx_Amount() + " " + configClass.Currency + "\n" +
                    "Fecha Operaci�n: " + cpIntegraEMV.getRspDate();
                    Application.Services.Dialog.ShowMessage(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtVoucher = cpIntegraEMV.getRspVoucher();

                    string validadorTarjeta = string.Empty;
                    validadorTarjeta = cpIntegraEMV.getCc_Type();
                    asignaCardTypeId(validadorTarjeta);

                    if (cpIntegraEMV.getRspVoucherCliente() != "")
                    {
                        VComercio = cpIntegraEMV.getRspVoucherComercio();
                        VCliente = cpIntegraEMV.getRspVoucherCliente() + cpIntegraEMV.getRspFeTxLeyenda(); //(Este parametro se usa solamente si se requiere factura electronica)
                    }
                    ImprimeVoucher();
                    status = 2;
                    decimal totalPayed = decimal.Parse(cpIntegraEMV.getTx_Amount().Replace(",", ""));
                    SaveTransaction(totalPayed, status);

                    break;

                case "denied":
                    TransaccionCancelada = false;
                    respuesta =
                        "          Cancelaci�n declinada" + "\n" +
                        "No se realiz� ning�n abono a su tarjeta." + "\n" +
                        "La operaci�n fue Declinada por su Banco Emisor. \n" +
                         cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.getRspDsError();
                    Application.Services.Dialog.ShowMessage(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;

                case "error":
                    TransaccionCancelada = false;
                    respuesta =
                         "               Error \n" +
                         "Ocurri� un error al realizar la cancelaci�n \n" +
                         "No se realiz� ning�n reintegro a su tarjeta \n" +
                         "Favor de intentarlo m�s tarde. \n" +
                         cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.getRspDsError();
                    Application.Services.Dialog.ShowMessage(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;

                default:
                    TransaccionCancelada = false;
                    respuesta =
                        "               Error \n" +
                        "Ocurri� un error al realizar la cancelaci�n \n" +
                        "No se realiz� ning�n reintegro a su tarjeta \n" +
                        "Favor de intentarlo m�s tarde. \n" +
                        cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.chkPp_DsError;
                    Application.Services.Dialog.ShowMessage(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;
            }
        }

        private void VerificaRespuestaCancelacionXML(XmlDocument xmlRetVal)
        {
            string respuesta = string.Empty;
            string txtVoucher = string.Empty;
            string XMLVoucher = string.Empty;
            XmlNode responseVenta = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_response");

            XmlNode nu_operaion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_operaion");
            XmlNode cd_usuario = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_usuario");
            XmlNode cd_empresa = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_empresa");
            XmlNode nu_sucursal = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_sucursal");
            XmlNode nu_afiliacion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_afiliacion");
            XmlNode nb_referencia = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_referencia");
            XmlNode cc_nombre = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cc_nombre");
            XmlNode cc_num = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cc_num");
            XmlNode cc_tp = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cc_tp");
            XmlNode nu_importe = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_importe");
            XmlNode cd_tipopago = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_tipopago");
            XmlNode cd_tipocobro = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_tipocobro");
            XmlNode cd_instrumento = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_instrumento");
            XmlNode nb_response = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_response");
            XmlNode nu_auth = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_auth");
            XmlNode fh_registro = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/fh_registro");
            XmlNode fh_bank = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/fh_bank");
            XmlNode cd_usrtransaccion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_usrtransaccion");
            XmlNode tp_operacion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/tp_operacion");
            XmlNode nb_currency = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_currency");
            XmlNode cd_resp = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_resp");
            XmlNode nb_resp = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_resp");

            switch (responseVenta.InnerText)
            {
                case "approved":  //Transacci�n Aprobada
                    TransaccionCancelada = true;
                    respuesta = "      Cancelaci�n aprobada" + "\n" +
                     "N�m. de Referencia: " + nb_referencia.InnerText + "\n" +
                    "N�m. de Operaci�n: " + nu_operaion.InnerText + "\n" +
                    "N�m. de Autorizaci�n: " + nu_auth.InnerText + "\n" +
                    "Tipo de Tarjeta: " + cc_tp.InnerText + " " + cd_instrumento.InnerText + "\n" +
                    "Nombre TH: " + cc_nombre.InnerText + "\n" +
                    "N�m. Tarjeta: " + cc_num.InnerText + "\n" +
                    "Vencimiento: " + cpIntegraEMV.getCc_ExpMonth() + "/" + cpIntegraEMV.getCc_ExpYear() + "\n" +
                    "Monto Cobro: $" + nu_importe.InnerText + " " + configClass.Currency + "\n" +
                    "Fecha Operaci�n: " + fh_bank.InnerText;
                    Application.Services.Dialog.ShowMessage(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    string validadorTarjeta = string.Empty;
                    validadorTarjeta = cc_tp.InnerText;
                    asignaCardTypeId(validadorTarjeta);

                    XMLVoucher = cpIntegraEMV.sndReimpresion(configClass.User, configClass.Pass, configClass.Company, configClass.Branch, configClass.Country, nu_operaion.InnerText);
                    XmlDocument xmlDocVoucherComercio = new XmlDocument();
                    XmlDocument xmlDocVoucherCliente = new XmlDocument();
                    if (!string.IsNullOrEmpty(XMLVoucher))
                    {
                        string VoucherComercio = XMLVoucher.Substring(XMLVoucher.IndexOf("<voucher_comercio>"), XMLVoucher.IndexOf("</voucher_comercio>") + 19);
                        xmlDocVoucherComercio.LoadXml(VoucherComercio);
                        string VoucherCliente = XMLVoucher.Substring(XMLVoucher.IndexOf("<voucher_cliente>"), (XMLVoucher.IndexOf("</voucher_cliente>") + 18 - (XMLVoucher.IndexOf("<voucher_cliente>"))));
                        xmlDocVoucherCliente.LoadXml(VoucherCliente);
                        VComercio = xmlDocVoucherComercio.InnerText;
                        VCliente = xmlDocVoucherCliente.InnerText;
                        ImprimeVoucher();
                    }

                    status = 2;
                    decimal totalPayed = decimal.Parse(nu_importe.InnerText.Replace(",", ""));
                    SaveTransactionFromXML(totalPayed, status, cd_instrumento.InnerText, nu_auth.InnerText, nb_currency.InnerText, nu_operaion.InnerText, nb_referencia.InnerText, fh_bank.InnerText, cc_nombre.InnerText, cc_num.InnerText, tp_operacion.InnerText);
                    CancelarOperacion();
                    break;

                case "denied":
                    TransaccionCancelada = false;
                    respuesta =
                        "          Cancelaci�n declinada" + "\n" +
                        "No se realiz� ning�n reintegro a su tarjeta." + "\n" +
                        "La operaci�n fue Declinada por su Banco Emisor. \n" +
                          nb_resp.InnerText;
                    Application.Services.Dialog.ShowMessage(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;

                case "error":
                    TransaccionCancelada = false;
                    respuesta =
                         "               Error \n" +
                         "Ocurri� un error al realizar la cancelaci�n \n" +
                         "No se realiz� ning�n reintegro a su tarjeta \n" +
                          nb_resp.InnerText;
                    Application.Services.Dialog.ShowMessage(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;

                default:
                    TransaccionCancelada = false;
                    respuesta =
                        "               Error \n" +
                        "Ocurri� un error al realizar la cancelaci�n \n" +
                        "No se realiz� ning�n reintegro a su tarjeta \n" +
                        nb_resp.InnerText;
                    Application.Services.Dialog.ShowMessage(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;
            }
        }

        private void SaveTransaction(decimal amount, int status)
        {
            bool retval = false;
            retval = odac.GuardaPagoTarjeta(configClass.User, Transaction.Shift.StaffId, cpIntegraEMV.chkCc_Name, cpIntegraEMV.chkCc_Number.Replace(" ", ""), " ", cpIntegraEMV.getCc_Type(), amount.ToString().Replace(",", ""), cpIntegraEMV.getRspAuth(), configClass.Currency, cpIntegraEMV.getRspDsMerchant(), cpIntegraEMV.getRspDsOperationType(), cpIntegraEMV.getRspOperationNumber(), cpIntegraEMV.getTx_Reference(), cpIntegraEMV.getRspDate(), DateTime.Now.ToString("HHmmss"), Application.Settings.Database.DataAreaID, Transaction.StoreId, Transaction.Shift.TerminalId, Transaction.TransactionId, 1, tlGlobal.LineId, Transaction.Shift.BatchId);
            if (retval == false)
            {
                Application.Services.Dialog.ShowMessage("Error al guardar la transacci�n, favor de contactar con sistemas, la operaci�n continuar�.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            odac.GuardaDetalletarjeta(ApplicationSettings.Database.StoreID, ApplicationSettings.Database.DATAAREAID, Transaction.Shift.TerminalId, Transaction.TenderLines.Last.Value.LineId, tlGlobal.TenderTypeId, cardTypeId, Transaction.TransactionId);
        }

        private void ImprimeVoucher()
        {
            PrintClass print = new PrintClass(Application);
            //Preguntar por Pin Pad con impresora integrada
            if (cpIntegraEMV.chkPp_Printer == "1")
            {
                if (!string.IsNullOrEmpty(cpIntegraEMV.getRspVoucherCliente()) && cpIntegraEMV.getRspVoucherCliente() != "")
                {
                    cpIntegraEMV.dbgPrintVoucher(cpIntegraEMV.getRspVoucher());
                }
            }
            else
            {
                if (VComercio != "")
                {
                    if (formatedXML) { print.ImprimeTicketFromXML(VComercio); }
                    else { print.ImprimeTicket(VComercio); }
                }
                if (VCliente != "")
                {
                    if (formatedXML) { print.ImprimeTicketFromXML(VCliente); }
                    else { print.ImprimeTicket(VCliente); }
                }
            }
            //DialogResult dialogResult = apps.Services.Dialog.ShowMessage(
            //           "�Desea imprimir el ticket en la impresora del POS?",
            //           MessageBoxButtons.YesNo,
            //           MessageBoxIcon.Question);
            //if (dialogResult == DialogResult.Yes)
            //{
            //    if (VComercio != "")
            //    {
            //        if (formatedXML) { print.ImprimeTicketFromXML(VComercio); }
            //        else { print.ImprimeTicket(VComercio); }
            //    }
            //    if (VCliente != "")
            //    {
            //        if (formatedXML) { print.ImprimeTicketFromXML(VCliente); }
            //        else { print.ImprimeTicket(VCliente); }
            //    }
            //}
        }

        private void SaveTransactionFromXML(decimal amount, int status, string cd_instrumento, string nu_auth, string nb_currency, string nu_operaion, string nb_referencia, string fh_bank, string cc_nombre, string cc_num, string tp_operacion)
        {

            bool retval = false;

            if (string.IsNullOrEmpty(respuestaMerchant))
            {
                respuestaMerchant = configClass.Tp_operacion;
            }

            string tipoOperacion = string.Empty;
            if (!string.IsNullOrEmpty(tp_operacion))
            {
                tipoOperacion = tp_operacion.Substring(0, 3);
            }
            else
            {
                tipoOperacion = "CAN";
            }
            retval = odac.GuardaPagoTarjeta(configClass.User, Transaction.Shift.StaffId, cc_nombre, cc_num.Replace(" ", ""), cpIntegraEMV.getCc_ExpMonth() + "/" + cpIntegraEMV.getCc_ExpYear(), cd_instrumento, amount.ToString().Replace(",", ""), nu_auth, nb_currency, respuestaMerchant, tipoOperacion, nu_operaion, nb_referencia, fh_bank, DateTime.Now.ToString("HHmmss"), Application.Settings.Database.DataAreaID, Transaction.StoreId, Transaction.Shift.TerminalId, Transaction.TransactionId, 1, tlGlobal.LineId, Transaction.Shift.BatchId);
            if (retval == false)
            {
                Application.Services.Dialog.ShowMessage("Error al guardar la transacci�n, favor de contactar con sistemas, la operaci�n continuar�.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            odac.GuardaDetalletarjeta(ApplicationSettings.Database.StoreID, ApplicationSettings.Database.DATAAREAID, Transaction.Shift.TerminalId, Transaction.TenderLines.Last.Value.LineId, tlGlobal.TenderTypeId, cardTypeId, Transaction.TransactionId);
        }

        private void CargaFormasPago(IApplication appli)
        {
            odac = new DAC(appli.Settings.Database.Connection);
            DtFormas = odac.getCardDetailPayments(appli.Shift.StoreId, appli.Settings.Database.DataAreaID, tenderTypeId);
        }

        private void asignaCardTypeId(string validadorTarjeta)
        {
            try
            {
                cardTypeId = formaPagoCredito;
                if (validadorTarjeta.Contains("AMEX") && !validadorTarjeta.Contains("BANAMEX"))
                {
                    cardTypeId = formaPagoAmex;
                }
                else
                {
                    if (validadorTarjeta.ToUpper().Contains("CREDITO"))
                    {
                        cardTypeId = formaPagoCredito;
                    }
                    else if (validadorTarjeta.ToUpper().Contains("CREDITO"))
                    {
                        cardTypeId = formaPagoDebito;
                    }
                    else if (!validadorTarjeta.ToUpper().Contains("DEBITO") && !validadorTarjeta.ToUpper().Contains("CREDITO"))
                    {
                        DialogResult dialogResult = Application.Services.Dialog.ShowMessage(
                                   "�Es tarjeta de CR�DITO?",
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            cardTypeId = formaPagoCredito;
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            cardTypeId = formaPagoDebito;
                        }
                    }
                }
                //if (DtFormas.Rows.Count > 0)
                //{
                //    //Set default
                //    foreach (DataRow row in DtFormas.Rows)
                //    {
                //        if (row["Descripcion"].ToString().ToUpper().Contains("SANTANDER CR�DITO"))
                //        {
                //            cardTypeId = row["CardTypeId"].ToString();
                //            break;
                //        }
                //    }
                //    if (validadorTarjeta.Contains("AMEX") && !validadorTarjeta.Contains("BANAMEX"))
                //    {
                //        foreach (DataRow row in DtFormas.Rows)
                //        {
                //            if (row["Descripcion"].ToString().ToUpper().Contains("AMERICAN"))
                //            {
                //                cardTypeId = row["CardTypeId"].ToString();
                //                break;
                //            }
                //        }
                //    }
                //    else
                //    {
                //        if (validadorTarjeta.ToUpper().Contains("CREDITO"))
                //        {
                //            foreach (DataRow row in DtFormas.Rows)
                //            {
                //                if (row["Descripcion"].ToString().ToUpper().Contains("SANTANDER CR�DITO"))
                //                {
                //                    cardTypeId = row["CardTypeId"].ToString();
                //                    break;
                //                }
                //            }
                //        }
                //        else if (validadorTarjeta.ToUpper().Contains("DEBITO"))
                //        {
                //            foreach (DataRow row in DtFormas.Rows)
                //            {
                //                if (row["Descripcion"].ToString().ToUpper().Contains("SANTANDER D�BITO"))
                //                {
                //                    cardTypeId = row["CardTypeId"].ToString();
                //                    break;
                //                }
                //            }
                //        }
                //        else if (!validadorTarjeta.ToUpper().Contains("DEBITO") && !validadorTarjeta.ToUpper().Contains("CREDITO"))
                //        {
                //            DialogResult dialogResult = apps.Services.Dialog.ShowMessage(
                //                       "�Es tarjeta de CR�DITO?",
                //                       MessageBoxButtons.YesNo,
                //                       MessageBoxIcon.Question);
                //            if (dialogResult == DialogResult.Yes)
                //            {
                //                foreach (DataRow row in DtFormas.Rows)
                //                {
                //                    if (row["Descripcion"].ToString().ToUpper().Contains("SANTANDER CR�DITO"))
                //                    {
                //                        cardTypeId = row["CardTypeId"].ToString();
                //                        break;
                //                    }
                //                }
                //            }
                //            else if (dialogResult == DialogResult.No)
                //            {
                //                foreach (DataRow row in DtFormas.Rows)
                //                {
                //                    if (row["Descripcion"].ToString().ToUpper().Contains("SANTANDER D�BITO"))
                //                    {
                //                        cardTypeId = row["CardTypeId"].ToString();
                //                        break;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                //else
                //{
                //    CreaMensaje("No se ha cargado la informaci�n de las formas de pago. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
            }
            catch (Exception ex)
            {
                Application.Services.Dialog.ShowMessage("Error al asignar la forma de pago. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool cargaFormasPago()
        {
            bool retval = false;
            try
            {
                odac = new DAC(Application.Settings.Database.Connection);
                DtFormas = odac.getCardTenderTypes(Application.Settings.Database.DataAreaID);
                #region Validate Voided TenderTypes
                if (DtFormas.Rows.Count > 0)
                {
                    foreach (DataRow dr in DtFormas.Rows)
                    {
                        if (dr["CARDTYPE"].ToString().ToUpperInvariant().Contains("CREDIT"))
                        {
                            formaPagoCredito = dr["RETAILCARDTYPEID"].ToString();
                        }
                        else if (dr["CARDTYPE"].ToString().ToUpperInvariant().Contains("DEBIT"))
                        {
                            formaPagoDebito = dr["RETAILCARDTYPEID"].ToString();
                        }
                        else if (dr["CARDTYPE"].ToString().ToUpperInvariant().Contains("AMEX") && !dr["CARDTYPE"].ToString().ToUpperInvariant().Contains("BANAMEX"))
                        {
                            formaPagoAmex = dr["RETAILCARDTYPEID"].ToString();
                        }
                    }
                    if (!string.IsNullOrEmpty(formaPagoCredito) && !string.IsNullOrEmpty(formaPagoDebito) && !string.IsNullOrEmpty(formaPagoAmex))
                        retval = true;
                    else
                    {
                        Application.Services.Dialog.ShowMessage("Error al cargar las formas de pago. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    Application.Services.Dialog.ShowMessage("Error al cargar las formas de pago. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Application.Services.Dialog.ShowMessage("Error al cargar las formas de pago.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Services.Dialog.ShowMessage("Descripci�n del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retval;
        }

        #endregion
    }
}
