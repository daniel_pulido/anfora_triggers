﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using LSRetailPosis.Settings;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using System.Collections;

namespace Microsoft.Dynamics.Retail.Pos.PaymentTriggers
{
    class DAC
    {
        private SqlConnection conn;
        public DAC(SqlConnection connData)
        {
            conn = connData;
        }

        public DataTable selectData(string consulta)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = consulta;
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public DataTable getCardPayments(string store, string DataAreaId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "select tt.TENDERTYPEID TenderId, tt.NAME DESCRIPCION from RETAILSTORETENDERTYPETABLE tt left join RETAILSTORETABLE s on tt.CHANNEL = s.RECID where tt.DATAAREAID = @DataAreaId and s.STORENUMBER = @Store and tt.FUNCTION_ = 1";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@Store", SqlDbType.NVarChar, 10).Value = store;
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 10).Value = DataAreaId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public DataTable getMITConfigCobranza(string StoreId, string TerminalId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "SELECT TOP 1 * FROM [ax].[GRW_MITCONFIG] WHERE StoreId = @StoreId AND Perfil = 3 AND Estatus = 1 AND TerminalId = @TerminalId ";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@StoreId", SqlDbType.NVarChar, 10).Value = StoreId;
                    command.Parameters.Add("@TerminalId", SqlDbType.NVarChar, 10).Value = TerminalId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public DataTable getMITConfigSuperAdmin(string StoreId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "SELECT TOP 1 * FROM [ax].[GRW_MITCONFIG] WHERE StoreId = @StoreId AND Perfil = 1 AND Estatus = 1 ";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@StoreId", SqlDbType.NVarChar, 10).Value = StoreId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public DataTable getMITConfigAdmin(string StoreId, string TerminalId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "SELECT TOP 1 * FROM [ax].[GRW_MITCONFIG] WHERE StoreId = @StoreId AND Perfil = 3 AND Estatus = 1 AND TerminalId = @TerminalId ";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@StoreId", SqlDbType.NVarChar, 10).Value = StoreId;
                    command.Parameters.Add("@TerminalId", SqlDbType.NVarChar, 10).Value = StoreId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public bool GuardaPagoTarjeta(string Bs_User, string Bs_UsrTransaction, string Cc_Name, string Cc_Number, string Cc_ExpDate, string Cc_Type, string Tx_Amount, string Tx_Auth, string Tx_Currency, string Tx_Merchant, string Tx_OperationType, string Tx_OperationNumber, string Tx_Reference, string Tx_Date, string HoraPOS, string DataAreaId, string StoreId, string Terminal, string TransactionId, int EstatusTran, int LineId, long BatchId)
        {
            SqlConnection connection = LSRetailPosis.Settings.ApplicationSettings.Database.LocalConnection;
            string sqlstr = string.Empty;

            sqlstr = "Exec xsp_GRW_INSERTMITTRAN @Bs_User, @Bs_UsrTransaction, @Cc_Name, @Cc_Number, @Cc_ExpDate, @Cc_Type, @Tx_Amount, @Tx_Auth, @Tx_Currency, @Tx_Merchant, @Tx_OperationType, @Tx_OperationNumber, @Tx_Reference, @Tx_Date, @HoraPOS, @DataAreaId, @StoreId, @Terminal, @TransactionId, @EstatusTran, @LineId, @BatchId";

            DataTable TranTable = new DataTable();
            try
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(sqlstr, conn))
                {
                    command.Parameters.Add("@Bs_User", SqlDbType.NVarChar, 10).Value = Bs_User;
                    command.Parameters.Add("@Bs_UsrTransaction", SqlDbType.NVarChar, 20).Value = Bs_UsrTransaction;
                    command.Parameters.Add("@Cc_Name", SqlDbType.NVarChar, 40).Value = Cc_Name;
                    command.Parameters.Add("@Cc_Number", SqlDbType.NVarChar, 16).Value = Cc_Number;
                    command.Parameters.Add("@Cc_ExpDate", SqlDbType.NVarChar, 10).Value = Cc_ExpDate;
                    command.Parameters.Add("@Cc_Type", SqlDbType.NVarChar, 8).Value = Cc_Type;
                    command.Parameters.Add("@Tx_Amount", SqlDbType.NVarChar, 12).Value = Tx_Amount;
                    command.Parameters.Add("@Tx_Auth", SqlDbType.NVarChar, 10).Value = Tx_Auth;
                    command.Parameters.Add("@Tx_Currency", SqlDbType.NVarChar, 3).Value = Tx_Currency;
                    command.Parameters.Add("@Tx_Merchant", SqlDbType.NVarChar, 10).Value = Tx_Merchant;
                    command.Parameters.Add("@Tx_OperationType", SqlDbType.NVarChar, 3).Value = Tx_OperationType;
                    command.Parameters.Add("@Tx_OperationNumber", SqlDbType.NVarChar, 15).Value = Tx_OperationNumber;
                    command.Parameters.Add("@Tx_Reference", SqlDbType.NVarChar, 35).Value = Tx_Reference;
                    command.Parameters.Add("@Tx_Date", SqlDbType.NVarChar, 10).Value = Tx_Date;
                    command.Parameters.Add("@HoraPOS", SqlDbType.NVarChar, 10).Value = HoraPOS;
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 4).Value = DataAreaId;
                    command.Parameters.Add("@StoreId", SqlDbType.NVarChar, 10).Value = StoreId;
                    command.Parameters.Add("@Terminal", SqlDbType.NVarChar, 10).Value = Terminal;
                    command.Parameters.Add("@TransactionId", SqlDbType.NVarChar, 44).Value = TransactionId;
                    command.Parameters.Add("@EstatusTran", SqlDbType.Int).Value = EstatusTran;
                    command.Parameters.Add("@LineId", SqlDbType.Int).Value = LineId;
                    command.Parameters.Add("@BatchId", SqlDbType.BigInt).Value = BatchId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        TranTable.Load(reader);
                    }
                }
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return true;
        }

        public void GuardaDetalletarjeta(string store, string DataAreaId, string Terminal, int lineNum, string tenderTypeId, string cardTypeId, string TransactionId)
        {
            try
            {
                string cmdText = "Exec xsp_insertDetailTenders @Store, @DataAreaId, @TenderTypeId, @LineNum, @CardTypeId, @TerminalId, @TransactionId";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@Store", SqlDbType.NVarChar, 10).Value = store;
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 10).Value = DataAreaId;
                    command.Parameters.Add("@TenderTypeId", SqlDbType.NVarChar, 10).Value = tenderTypeId;
                    command.Parameters.Add("@LineNum", SqlDbType.Int).Value = lineNum;
                    command.Parameters.Add("@CardTypeId", SqlDbType.NVarChar, 10).Value = cardTypeId;
                    command.Parameters.Add("@TerminalId", SqlDbType.NVarChar, 10).Value = Terminal;
                    command.Parameters.Add("@TransactionId", SqlDbType.NVarChar, 44).Value = TransactionId;
                    command.ExecuteReader();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }

        public DataTable getCardDetailPayments(string store, string DataAreaId, string tenderTypeId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "Exec xsp_getCardDetailTenders @Store, @DataAreaId, @TenderTypeId";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@Store", SqlDbType.NVarChar, 10).Value = store;
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 10).Value = DataAreaId;
                    command.Parameters.Add("@TenderTypeId", SqlDbType.NVarChar, 10).Value = tenderTypeId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public DataTable getCardTenderTypes(string DataAreaId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = " SELECT [CARDTYPE], [RETAILCARDTYPEID] FROM [ax].[GRW_CONFIGTARJBANC] WHERE DATAAREAID = @DataAreaId AND PROVIDER = 'MIT'";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 10).Value = DataAreaId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }
    }
}
