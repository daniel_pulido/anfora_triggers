﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Dynamics.Retail.Pos.ApplicationTriggers
{
    public class FormatString
    {
         //CONSTANTES

        #region "VARIABLES"
        string oCodigoTransaccion = string.Empty;
        string oNumTerminal = string.Empty;
        string oNumSesion = string.Empty;
        string oNumSecTrans = string.Empty;
        string oImporteTrans = string.Empty;
        string oFiller01 = string.Empty;
        string oFolio = string.Empty;
        string oCapacidadEMV = string.Empty;
        string oTipoLectorTarjeta = string.Empty;
        string oCapacidadCVV2 = string.Empty;
        string oMesesFinanciamiento = string.Empty;
        string oParcializacionPagos = string.Empty;
        string oPromociones = string.Empty;
        string oTipoModena = string.Empty;
        string oAutorizacion = string.Empty;
        string oModoIngresoTar = string.Empty;
        string oCCV2 = string.Empty;
        string oTrackII = string.Empty;
        string oNumSec = string.Empty;
        string oImporteCash = string.Empty;
        string oFechaHoraComercio = string.Empty;
        string dateNow = DateTime.Now.ToString("yyyyMMddHHmmss");
        string oRefCom = string.Empty;
        string oImporte2 = string.Empty;
        string oClaveOperador = string.Empty;
        string oAfiliacion = string.Empty;
        string oFiller02 = string.Empty;
        string oRefFinanciera = string.Empty;
        string oFiller03 = string.Empty;
        string oFiller04 = string.Empty;
        string oFiller05 = string.Empty;
        string oLongPagos = string.Empty;
        string oDatosVar = string.Empty;
        string oIdentificador = string.Empty;
        
        #endregion

        #region "CONSTRUCTORES Y DESTRUCTOR"
        public FormatString()
        {


        }
        #endregion

        #region "MÉTODOS TRANSACCIONES"

        public string ventaForzada(string nAfiliacion, string nTerminal, int nSesion, int nTransaccion, int nImporte, int nTipoMoneda, string cveUsuario, int MesesFinanciamiento, int ParcializacionPagos, int Promociones, string NoAut)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "901";
                oNumTerminal = formatStringZeroValues(nTerminal, 8);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(nImporte, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(0, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "1";
                oMesesFinanciamiento = formatZeroValues(MesesFinanciamiento, 2);
                oParcializacionPagos = formatZeroValues(ParcializacionPagos, 2);
                oPromociones = formatZeroValues(Promociones, 2);
                oTipoModena = nTipoMoneda.ToString();
                oAutorizacion = NoAut;
                oModoIngresoTar = formatEmptyValues(2);
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues(cveUsuario.ToUpper(), 6);
                oAfiliacion = formatStringZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatZeroValues(0, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);
                oDatosVar = formatZeroValues(0, 115);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos;
            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;
        }

        public string venta(string nAfiliacion, string nTerminal, int nSesion, int nTransaccion, int nImporte, int nTipoMoneda, string cveUsuario, int MesesFinanciamiento, int ParcializacionPagos, int Promociones)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "001";
                oNumTerminal = formatStringZeroValues(nTerminal, 8);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(nImporte, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(0, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "1";
                oMesesFinanciamiento = formatZeroValues(MesesFinanciamiento, 2);
                oParcializacionPagos = formatZeroValues(ParcializacionPagos, 2);
                oPromociones = formatZeroValues(Promociones, 2);
                oTipoModena = nTipoMoneda.ToString();
                oAutorizacion = formatEmptyValues(6);
                oModoIngresoTar = formatEmptyValues(2);
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues(cveUsuario.ToUpper(), 6);
                oAfiliacion = formatStringZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatZeroValues(0, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);
                oDatosVar = formatZeroValues(0, 115);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos;
            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;
        }

        public string consultaPuntos(string nAfiliacion, string nTerminal, int nSesion, int nTransaccion, int nImporte, int nTipoMoneda, string cveUsuario, int MesesFinanciamiento, int ParcializacionPagos, int Promociones)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "016";
                oNumTerminal = formatStringZeroValues(nTerminal, 8);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(nImporte, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(0, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "1";
                oMesesFinanciamiento = formatZeroValues(MesesFinanciamiento, 2);
                oParcializacionPagos = formatZeroValues(ParcializacionPagos, 2);
                oPromociones = formatZeroValues(Promociones, 2);
                oTipoModena = nTipoMoneda.ToString();
                oAutorizacion = formatEmptyValues(6);
                oModoIngresoTar = formatEmptyValues(2);
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues(cveUsuario.ToUpper(), 6);
                oAfiliacion = formatStringZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatZeroValues(0, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);
                oDatosVar = formatZeroValues(0, 115);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos;
            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;
        }

        public string devolucion(string nAfiliacion, string nTerminal, int nSesion, int nTransaccion, int nImporte, int nTipoMoneda, string cveUsuario, string nAutorizacion, string nRefFinanciera)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "002";
                oNumTerminal = formatStringZeroValues(nTerminal, 8);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(nImporte, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(0, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "1";
                oMesesFinanciamiento = formatZeroValues(0, 2);
                oParcializacionPagos = formatZeroValues(0, 2);
                oPromociones = formatZeroValues(0, 2);
                oTipoModena = nTipoMoneda.ToString();
                oAutorizacion = formatEmptyValues(6);
                oModoIngresoTar = formatEmptyValues(2);
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues(cveUsuario.ToUpper(), 6);
                oAfiliacion = formatStringZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatStringZeroValues(nRefFinanciera, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);
                oDatosVar = formatZeroValues(0, 115);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos;
            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;

        }

        public string cancelacion(string nAfiliacion, string nTerminal, int nSesion, int nTransaccion, int nImporte, int nTipoMoneda, string cveUsuario, string nAutorizacion, string nRefFinanciera)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "201";
                oNumTerminal = formatStringZeroValues(nTerminal, 8);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(nImporte, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(0, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "1";
                oMesesFinanciamiento = formatZeroValues(0, 2);
                oParcializacionPagos = formatZeroValues(0, 2);
                oPromociones = formatZeroValues(0, 2);
                oTipoModena = nTipoMoneda.ToString();
                oAutorizacion = formatStringZeroValues(nAutorizacion, 6);
                oModoIngresoTar = formatEmptyValues(2);
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues(cveUsuario.ToUpper(), 6);
                oAfiliacion = formatStringZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatStringZeroValues(nRefFinanciera, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);
                oDatosVar = formatZeroValues(0, 115);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos;
            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;

        }
        
        #endregion

        #region "MÉTODOS SESION OPERADOR"

        public string abrirSesionTarjetaOperadorST(int nAfiliacion, int nSucursal, int nTerminal, int nSesion, int nTransaccion, string cveUsuario)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "070";
                oNumTerminal = formatZeroValues(nSucursal, 4) + formatZeroValues(nTerminal, 4);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(0, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(1, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "8";
                oMesesFinanciamiento = formatZeroValues(0, 2);
                oParcializacionPagos = formatZeroValues(0, 2);
                oPromociones = formatZeroValues(0, 2);
                oTipoModena = "0";
                oAutorizacion = formatEmptyValues(6);
                oModoIngresoTar = formatEmptyValues(2);
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues(cveUsuario.ToUpper(), 6);
                oAfiliacion = formatZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatZeroValues(0, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos + "03";

            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;

        }

        public string abrirSesionTarjetaOperadorCT(int nAfiliacion, int nSucursal, int nTerminal, int nSesion, int nTransaccion, string cveUsuario)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "070";
                oNumTerminal = formatZeroValues(nSucursal, 4) + formatZeroValues(nTerminal, 4);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(0, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(1, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "8";
                oMesesFinanciamiento = formatZeroValues(0, 2);
                oParcializacionPagos = formatZeroValues(0, 2);
                oPromociones = formatZeroValues(0, 2);
                oTipoModena = "0";
                oAutorizacion = formatEmptyValues(6);
                oModoIngresoTar = formatEmptyValues(2);
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues(cveUsuario.ToUpper(), 6);
                oAfiliacion = formatZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatZeroValues(0, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos + "01";

            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;

        }

        public string cerrarSesionTarjetaOperador(int nAfiliacion, int nSucursal, int nTerminal, int nSesion, int nTransaccion, string cveUsuario)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "070";
                oNumTerminal = formatZeroValues(nSucursal, 4) + formatZeroValues(nTerminal, 4);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(0, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(1, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "8";
                oMesesFinanciamiento = formatZeroValues(0, 2);
                oParcializacionPagos = formatZeroValues(0, 2);
                oPromociones = formatZeroValues(0, 2);
                oTipoModena = "0";
                oAutorizacion = formatEmptyValues(6);
                oModoIngresoTar = formatEmptyValues(2);
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues(cveUsuario.ToUpper(), 6);
                oAfiliacion = formatZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatZeroValues(0, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos + "00";

            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;

        }

        #endregion

        #region "MÉTODOS PINPAD"

        public string cargaLlaves(string nAfiliacion, string nTerminal, int nSesion, int nTransaccion)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "092";
                oNumTerminal = formatStringZeroValues(nTerminal, 8);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(0, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(1, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "0";
                oMesesFinanciamiento = formatZeroValues(0, 2);
                oParcializacionPagos = formatZeroValues(0, 2);
                oPromociones = formatZeroValues(0, 2);
                oTipoModena = "0";
                oAutorizacion = formatEmptyValues(6);
                oModoIngresoTar = "01";
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues("34", 6);
                oAfiliacion = formatStringZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatZeroValues(0, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos;
            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;
        }

        public string cargaLlavesOtroAdquiriente(string nAfiliacion, string nTerminal, int nSesion, int nTransaccion)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "070";
                oNumTerminal = formatStringZeroValues(nTerminal, 8);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(0, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(1, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "0";
                oMesesFinanciamiento = formatZeroValues(0, 2);
                oParcializacionPagos = formatZeroValues(0, 2);
                oPromociones = formatZeroValues(0, 2);
                oTipoModena = "0";
                oAutorizacion = formatEmptyValues(6);
                oModoIngresoTar = "01";
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues("34", 6);
                oAfiliacion = formatStringZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatZeroValues(0, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);
                oIdentificador = formatZeroValues(2, 2);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos + oIdentificador;
            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;
        }

        public string telecarga(string nAfiliacion, string nTerminal, int nSesion, int nTransaccion)
        {
            string oSetRequest = string.Empty;
            try
            {
                oCodigoTransaccion = "070";
                oNumTerminal = formatStringZeroValues(nTerminal, 8);
                oNumSesion = formatZeroValues(nSesion, 4);//Pendiente definir como se usarán
                oNumSecTrans = formatZeroValues(nTransaccion, 4);//Pendiente definir como se usarán
                oImporteTrans = formatZeroValues(0, 12);
                oFiller01 = formatZeroValues(0, 12);
                oFolio = formatZeroValues(1, 7);
                oCapacidadEMV = "3";
                oTipoLectorTarjeta = "8";
                oCapacidadCVV2 = "0";
                oMesesFinanciamiento = formatZeroValues(0, 2);
                oParcializacionPagos = formatZeroValues(0, 2);
                oPromociones = formatZeroValues(0, 2);
                oTipoModena = "0";
                oAutorizacion = formatEmptyValues(6);
                oModoIngresoTar = "01";
                oCCV2 = formatEmptyValues(4);
                oTrackII = formatEmptyValues(40);
                oNumSec = formatEmptyValues(3);
                oImporteCash = formatZeroValues(0, 12);
                oFechaHoraComercio = dateNow.Substring(2);
                oRefCom = formatEmptyValues(45);
                oImporte2 = formatZeroValues(0, 12);
                oClaveOperador = formatEmptyAlphaValues("34", 6);
                oAfiliacion = formatStringZeroValues(nAfiliacion, 8);
                oFiller02 = formatEmptyValues(4);
                oRefFinanciera = formatZeroValues(0, 8);
                oFiller03 = formatZeroValues(0, 1);
                oFiller04 = formatZeroValues(0, 3);
                oFiller05 = formatZeroValues(0, 3);
                oLongPagos = formatZeroValues(0, 4);
                oIdentificador = formatZeroValues(5, 2);

                oSetRequest = oCodigoTransaccion + oNumTerminal + oNumSesion + oNumSecTrans + oImporteTrans +
                oFiller01 + oFolio + oCapacidadEMV + oTipoLectorTarjeta + oCapacidadCVV2 +
                oMesesFinanciamiento + oParcializacionPagos + oPromociones + oTipoModena + oAutorizacion + oModoIngresoTar + oCCV2 + oTrackII + oNumSec + oImporteCash + oFechaHoraComercio + oRefCom +
                oImporte2 + oClaveOperador + oAfiliacion + oFiller02 + oRefFinanciera + oFiller03 +
               oFiller04 + oFiller05 + oLongPagos + oIdentificador;
            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
                oSetRequest = "ERROR";
            }
            return oSetRequest;
        }

        #endregion

        #region "METODOS AUXILIARES"

        /// <summary>
        /// Método que recibe el valor a formatear con ceros a la izquierda y la cantidad de caracteres que debe contener, el resultado es un string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cZero"></param>
        /// <returns></returns>
        private string formatZeroValues(int value, int cZero)
        {
            string formatedValue = string.Empty;
            string zeroCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cZero; i++)
                {
                    zeroCharacters += "0";
                }

                formatedValue = zeroCharacters + value.ToString();
                formatedValue = formatedValue.Substring(formatedValue.Length - cZero);

            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
            }
            return formatedValue;
        }

        private string formatStringZeroValues(string value, int cZero)
        {
            string formatedValue = string.Empty;
            string zeroCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cZero; i++)
                {
                    zeroCharacters += "0";
                }

                formatedValue = zeroCharacters + value;
                formatedValue = formatedValue.Substring(formatedValue.Length - cZero);

            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
            }
            return formatedValue;
        }

        /// <summary>
        ///  Método que recibe la cantidad de valores caracteres vacíos que debe regresar en formato string.
        /// </summary>
        /// <param name="cEmpty"></param>
        /// <returns></returns>
        private string formatEmptyValues(int cEmpty)
        {
            string formatedValue = string.Empty;
            string zeroCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cEmpty; i++)
                {
                    zeroCharacters += " ";
                }

                formatedValue = zeroCharacters;
            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
            }
            return formatedValue;
        }
        /// <summary>
        /// Método que recibe el valor a formatear con espacios vacíos a la izquierda y la cantidad de caracteres que debe contener, el resultado es un string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cEmpty"></param>
        /// <returns></returns>
        private string formatEmptyAlphaValues(string value, int cEmpty)
        {
            string formatedValue = string.Empty;
            string zeroCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cEmpty; i++)
                {
                    zeroCharacters += " ";
                }

                formatedValue = zeroCharacters + value.ToString();
                formatedValue = formatedValue.Substring(formatedValue.Length - cEmpty);
            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
            }
            return formatedValue;
        }

        #endregion
    }
    
}
