﻿using LSRetailPosis;
using LSRetailPosis.DataAccess;
using LSRetailPosis.POSProcesses.WinFormsTouch;
using LSRetailPosis.POSProcesses;
using LSRetailPosis.Settings;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.Transaction;
using Microsoft.Dynamics.Retail.Notification.Contracts;
using Microsoft.Dynamics.Retail.Pos.Contracts.UI;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.ComponentModel.Composition;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System;
using System.IO;
using EglobalBBVA;
using System.Text;

namespace Microsoft.Dynamics.Retail.Pos.ApplicationTriggers
{
    class OperacionesAplliNET
    {
        #region PROPERTIES
        bool cargaPinPadCorrecta = false;
        private RetailTransaction Transaction;
        private IApplication apps;
        string cveUsuario = string.Empty;
        //AppliNET
        PinPadSC Enviar = new PinPadSC();
        String venta = String.Empty;
        String devolucion = String.Empty;
        String cancelacion_venta = String.Empty;
        String cancelacion_devolucion = String.Empty;
        String venta_forzada = String.Empty;
        String consulta_puntos = String.Empty;
        String checkin = String.Empty;
        String checkout = String.Empty;
        String cargocuarto = String.Empty;
        String reautorizacion = String.Empty;
        String checkout_reautorizado = String.Empty;
        String postpropina = String.Empty;
        String cancelacion_postpropina = String.Empty;
        String cancelacion_checkin = String.Empty;
        String cancelacion_checkout = String.Empty;
        String carga_llaves = String.Empty;
        String telecarga = String.Empty;
        String sintrj_operador = String.Empty;
        String abrir_sesion = String.Empty;
        String cerrar_sesion = String.Empty;
        String pago_tarjeta_efectivo = String.Empty;
        String traspaso_tarjetas = String.Empty;
        String Pago_CIE = String.Empty;
        String Pago_con_Tarjeta = String.Empty;
        String cuenta_express = String.Empty;
        String celular = String.Empty;
        String venta_amex = String.Empty;
        String BCargaLlave = String.Empty;//Para Controlar la Carga de Llaves por Bandera 1
        String strVersion = String.Empty;//Para guardar el password del certificado
        String BACT = String.Empty;//Para Controlar Actualizacion de Version PinPad
        String SA = String.Empty;
        int sesion;
        int transaccion;
        FormatString oFormatString = new FormatString();
        //Impresion
        private const int paperWidth = 55;
        StringBuilder reportLayout;
        private static readonly string singleLine = string.Empty.PadLeft(paperWidth, '-');
        private static readonly string lineFormat = ApplicationLocalizer.Language.Translate(7060);
        private static readonly string currencyFormat = ApplicationLocalizer.Language.Translate(7061);
        private static readonly string typeFormat = ApplicationLocalizer.Language.Translate(7062);
        #endregion

        #region CONSTRUCTOR

        public OperacionesAplliNET(RetailTransaction tran, IApplication appli)
        {
            apps = appli;
            Transaction = tran;
            //Inicializar variables para AppliNET
            try
            {
                sesion = Convert.ToInt32(Transaction.Shift.BatchId.ToString("000000"));
            }
            catch
            {
                sesion = 0;
            }
            try
            {
                transaccion = Convert.ToInt32(DateTime.Now.TimeOfDay.TotalSeconds);
            }
            catch (Exception ex)
            {
                transaccion = 0;
            }
        }

        #endregion

        #region MEMBERS

        /// <summary>
        /// Inicializa la PinPad y hace el proceso de atualización automática o carga de llaves si es requerido
        /// </summary>
        public void InicializaPinPad()
        {
            try
            {
                Enviar.SincronicacionInicial();
                if (!File.Exists(Application.StartupPath + "\\PinpadConfig.txt"))
                {
                    throw new Exception("Inicialización PinPad. No existe el archivo " + Application.StartupPath + "\\PinpadConfig.txt");
                }
                ClsConfiguracion.LeeDatosFromTXT();
                if (ClsConfiguracion.CargaLlave == 1)
                {
                    //Formato de la cadena
                    carga_llaves = oFormatString.cargaLlaves(ClsConfiguracion.Afiliacion, ClsConfiguracion.Terminal, sesion, transaccion);
                    Enviar.fncEglobalBBVA(carga_llaves, 0);
                    if (Enviar.RespDLL != 0)
                    {
                        if (Enviar.ClsResponse.C05_CodigoRespuesta == String.Empty)
                        {
                            CreaMensaje("Inicialización PinPad. "+Enviar.Response, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else if (Enviar.ClsResponse.C05_CodigoRespuesta != "00")
                        {
                            CreaMensaje("Inicialización PinPad. "+Enviar.ClsResponse.C16_Leyenda.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        if (Enviar.ClsResponse.ToString() != "")
                        {
                            CreaMensaje("Inicialización PinPad. "+Enviar.ClsResponse.C16_Leyenda, MessageBoxButtons.OK, MessageBoxIcon.Information);
                         
                        }
                    }
                }
                else if (ClsConfiguracion.BACTVersion == 1) //Para Actualizar la versión del PinPad Forma Automatica
                {
                    String CadEnviar = String.Empty;
                    DialogResult Respuesta;

                    Respuesta = MessageBox.Show("Inicialización PinPad. Se realizará actualización de versión");
                    if (Respuesta == DialogResult.OK)
                    {
                        //Formato de la cadena
                        CadEnviar = oFormatString.telecarga(ClsConfiguracion.Afiliacion, ClsConfiguracion.Terminal, sesion, transaccion);
                        Enviar.fncEglobalBBVA(CadEnviar, 0);
                        if (Enviar.RespDLL != 0)
                        {
                            if (Enviar.ClsResponse.C05_CodigoRespuesta == String.Empty)
                            {
                                CreaMensaje(Enviar.Response, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else if (Enviar.ClsResponse.C05_CodigoRespuesta != "00")
                            {
                                CreaMensaje(Enviar.ClsResponse.C16_Leyenda.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            if (Enviar.ClsResponse.ToString() != "")
                            {
                                CreaMensaje(Enviar.ClsResponse.C16_Leyenda.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                    else if (Respuesta == DialogResult.Cancel)
                    {
                        //No enviamos la solicitud al PinPad
                        //y continua con el proceso normal
                    }
                }
                SA = "0";
                BCargaLlave = "0";
                BACT = "0";
                File.Delete(Application.StartupPath + "/PinpadConfig.txt");
                using (StreamWriter w = File.AppendText(Application.StartupPath + "/PinpadConfig.txt"))
                {
                    w.WriteLine("IPHOST:" + ClsConfiguracion.IpHost);
                    w.WriteLine("SOCKETHOST:" + ClsConfiguracion.PuertoHost);
                    w.WriteLine("PINPADTIMEOUT:" + ClsConfiguracion.PinpadTimeOut);
                    w.WriteLine("NOMBREPUERTO:" + ClsConfiguracion.PuertoSerie);
                    w.WriteLine("PINPADID:" + ClsConfiguracion.PinPadID);
                    w.WriteLine("AFILIACION:" + ClsConfiguracion.Afiliacion);
                    w.WriteLine("TERMINAL:" + ClsConfiguracion.Terminal);
                    w.WriteLine("TIMEOUTHOST:" + ClsConfiguracion.HostTimeOut);
                    w.WriteLine("NOMBRECOMERCIO:" + ClsConfiguracion.Comercio);
                    w.WriteLine("HEADERCONSECUTIVO:" + ClsConfiguracion.HeaderConsecutivo);
                    w.WriteLine("FOLIO:" + ClsConfiguracion.IsFolio);
                    w.WriteLine("LOG:" + ClsConfiguracion.IsLog);
                    w.WriteLine("DIRUSERDB:" + ClsConfiguracion.DirUserDB);
                    w.WriteLine("BINESCOMERCIO:" + ClsConfiguracion.IDBinesComercio);
                    w.WriteLine("SESIONABRIR:" + SA);
                    w.WriteLine("CARGALLAVE:" + BCargaLlave);
                    w.WriteLine("VERSION:" + ClsConfiguracion.Password); //Solo si la DLL es por Internet
                    w.WriteLine("IMPRESIOND:" + ClsConfiguracion.Impresion);
                    w.WriteLine("ACTUALIZACION:" + BACT);
                    w.WriteLine("DIRACTPINPAD:" + ClsConfiguracion.DIRACTVersion);
                    w.WriteLine("CONAUX:" + ClsConfiguracion.ConAMEX);
                    w.WriteLine("CASH:" + ClsConfiguracion.Cash);
                    w.Flush();
                    w.Close();
                }
                cargaPinPadCorrecta = true;
            }
            catch (Exception ex)
            {
                CreaMensaje("Ocurrió un error al inicializar la PinPad AppliNET, reporte a sistemas. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CreaMensaje(string mensaje, MessageBoxButtons btn, MessageBoxIcon icon)
        {
            using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(mensaje, btn, icon))
            {
                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            }
        }

        /// <summary>
        /// Método que recibe el valor a formatear con ceros a la izquierda y la cantidad de caracteres que debe contener, el resultado es un string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cZero"></param>
        /// <returns></returns>
        private string formatZeroValues(int value, int cZero)
        {
            string formatedValue = string.Empty;
            string zeroCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cZero; i++)
                {
                    zeroCharacters += "0";
                }

                formatedValue = zeroCharacters + value.ToString();
                formatedValue = formatedValue.Substring(formatedValue.Length - cZero);

            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
            }
            return formatedValue;
        }

        private string formatStringZeroValues(string value, int cZero)
        {
            string formatedValue = string.Empty;
            string zeroCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cZero; i++)
                {
                    zeroCharacters += "0";
                }

                formatedValue = zeroCharacters + value;
                formatedValue = formatedValue.Substring(formatedValue.Length - cZero);

            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
            }
            return formatedValue;
        }

        #endregion
    }
}
